﻿namespace Anelesisso.Shared
{
    public static class OptionalExtensions
    {
        public static Optional<T> AsOptional<T>(this T obj)
        {
            if (obj is { })
                return Optional.Of(obj);
            else
                return Optional.Empty<T>();
        }
    }
}
