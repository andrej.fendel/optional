﻿using System;

namespace Anelesisso.Shared
{
    public sealed class Optional<TSource> : Optional
    {
        private readonly TSource value;

        private Optional()
        {
            value = default;
            HasValue = false;
        }

        private Optional(TSource value)
        {
            this.value = value;
            HasValue = value is object;
        }

        public override bool HasValue { get; }

        public TSource Value
        {
            get
            {
                if (HasValue)
                    return value;
                else
                    throw new InvalidOperationException();
            }
        }

        public override object ObjectValue => Value;

        public static Optional<TSource> Of(TSource value)
        {
            return new Optional<TSource>(value);
        }

        public static Optional<TSource> Empty()
        {
            return new Optional<TSource>();
        }

        public Optional<TResult> Select<TResult>(Func<TSource, TResult> selector)
        {
            return HasValue
                ? new(selector(value))
                : Empty<TResult>();
        }

        public Optional<TResult> SelectMany<TResult>(Func<TSource, Optional<TResult>> selector)
        {
            return HasValue
                ? selector(value)
                : Empty<TResult>();
        }

        public void Do(Action<TSource> doAction)
        {
            if (HasValue)
                doAction(value);
        }

        public void DoOrElse(Action<TSource> doAction, Action elseAction)
        {
            if (HasValue)
                doAction(value);
            else
                elseAction();
        }

        public TSource GetValueOrDefault()
        {
            return GetValueOrDefault(default);
        }

        public TSource GetValueOrDefault(TSource defaultValue)
        {
            return HasValue
                ? value
                : defaultValue;
        }


        public Optional<T> Cast<T>() where T : TSource
        {
            if (!HasValue)
                return Empty<T>();

            if (value is T)
                return Optional.Of((T)Convert.ChangeType(value, typeof(T)));
            else
                throw new InvalidCastException();
        }

        public override bool Equals(object other)
        {
            if (HasValue == false && other == null)
                return true;

            if (other is not Optional && other is not TSource)
                return false;

            object outherValue;
            if (other is Optional otherOptional)
            {
                if (HasValue ^ otherOptional.HasValue)
                    return false;

                if (!HasValue && !otherOptional.HasValue)
                    return otherOptional.GetType().GetGenericArguments()[0].IsAssignableTo(typeof(TSource));

                outherValue = otherOptional.ObjectValue;
            }
            else if (other is TSource source)
                outherValue = source;
            else
                return false;

            return Equals(value, outherValue);
        }

        public override int GetHashCode()
        {
            return HasValue
                ? value.GetHashCode()
                : 0;
        }

        public override string ToString()
        {
            return HasValue
                ? value.ToString()
                : string.Empty;
        }

        public static implicit operator Optional<TSource>(TSource value)
        {
            return Of(value);
        }

        public static explicit operator TSource(Optional<TSource> value)
        {
            if (value is null)
                return default;

            return value.Value;
        }
    }


    public abstract class Optional
    {
        protected Optional() { }

        public abstract object ObjectValue { get; }

        public abstract bool HasValue { get; }

        public static Optional<TSource> Of<TSource>(TSource value)
        {
            return Optional<TSource>.Of(value);
        }

        public static Optional<TSource> Empty<TSource>()
        {
            return Optional<TSource>.Empty();
        }
    }
}
