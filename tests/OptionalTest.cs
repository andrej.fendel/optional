using Anelesisso.Shared;
using Moq;
using NUnit.Framework;
using System;
using System.Collections;


namespace Tests.Anelesisso.Shared
{
    public class OptionalTest
    {
        [Test]
        public void CanCreate()
        {
            var sut = Optional.Of(5);
            Assert.That(sut, Is.InstanceOf<Optional<int>>());
        }

        [TestCaseSource(nameof(FirstFunctorLowTestCases))]
        public void FulfillFirstFunctorLow(Optional<object> optional)
        {
            var actual = optional.Select(Id);
            Assert.That(actual, Is.EqualTo(optional));
        }

        static IEnumerable FirstFunctorLowTestCases()
        {
            yield return new object[] { Optional.Of<object>(5) };
            yield return new object[] { Optional.Of<object>("Test") };
            yield return new object[] { Optional.Empty<object>() };
        }

        private T Id<T>(T value) => value;


        [TestCaseSource(nameof(SecondFunctorLowTestCases))]
        public void FulfilSecondFunctorLow(Optional<int> optional)
        {
            Func<int, string> f1 = x => x.ToString();
            Func<int, int> f2 = x => x + 5;
            var composed = Compose(f1, f2);

            var actual = optional.Select(composed);
            var expected = optional.Select(f2).Select(f1);

            Assert.That(actual, Is.EqualTo(expected));
        }

        static IEnumerable SecondFunctorLowTestCases()
        {
            yield return new object[] { Optional.Of(5) };
            yield return new object[] { Optional.Empty<int>() };

        }

        public Func<T1, T3> Compose<T1, T2, T3>(
            Func<T2, T3> func1,
            Func<T1, T2> func2)
            => x => func1(func2(x));

        [Test]
        public void GetHasValue_ValueIsPresent_ReturnsTrue()
        {
            var sut = Optional<string>.Of("Test");
            var actual = sut.HasValue;
            Assert.That(actual, Is.True);
        }

        [Test]
        public void GetHasVakue_ValueIsNotPresent_ReturnsFalse()
        {
            var sut = Optional.Of<string>(null);
            var actual = sut.HasValue;
            Assert.That(actual, Is.False);
        }

        [Test]
        public void Empty_ReferenceTypeIsUsed_ReturnsInstanceOfTypeOptional()
        {
            var actual = Optional.Empty<string>();
            Assert.That(actual, Is.InstanceOf<Optional<string>>());
        }

        [Test]
        public void Empty_ReferenceTypeIsUsed_ContainsNoValue()
        {
            var sut = Optional.Empty<string>();
            var actual = sut.HasValue;
            Assert.That(actual, Is.False);
        }

        [Test]
        public void Empty_ValueTypeIsUsed_ReturnsInstanceOfTypeOptional()
        {
            var actual = Optional.Empty<int>();
            Assert.That(actual, Is.InstanceOf<Optional<int>>());
        }

        [Test]
        public void Empty_ValueTypeIsUsed_ContainsNoValue()
        {
            var sut = Optional.Empty<int>();
            var actual = sut.HasValue;
            Assert.That(actual, Is.False);
        }

        [Test]
        public void SelectMany_HasValue_AsExpected()
        {
            var sut = Optional.Of("5");
            var actual = sut.SelectMany(TryParseInt);
            Assert.That(actual, Is.EqualTo(Optional.Of(5)));
        }

        private Optional<int> TryParseInt(string value)
        {
            return int.TryParse(value, out var result)
                ? Optional.Of(result)
                : Optional.Empty<int>();
        }

        [Test]
        public void SelectMany_HasNoValue_ReturnsEmptyOption()
        {
            var sut = Optional.Empty<string>();
            var actual = sut.SelectMany(x => Optional.Of(x.Length));
            Assert.That(actual.HasValue, Is.False);
        }

        [Test]
        public void GetValue_NoValueAvailable_ThrowsInvalidOperationException()
        {
            var sut = Optional.Empty<string>();
            Assert.That(() => sut.Value, Throws.InvalidOperationException);
        }

        [Test]
        public void GetValue_ValueAvailable_ReturnsValue()
        {
            var sut = Optional.Of("Test");
            var actual = sut.Value;
            Assert.That(actual, Is.EqualTo("Test"));
        }


        [Test]
        public void GetValueOrDefault_NoValueAvailableAndTypeIsReferenceType_ReturnsDefault()
        {
            var sut = Optional.Empty<string>();
            var actual = sut.GetValueOrDefault();
            Assert.That(actual, Is.Null);
        }

        [Test]
        public void GetValueOrDefault_NoValueAvailableAndTypeIsValueType_ReturnsDefault()
        {
            var sut = Optional.Empty<int>();
            var actual = sut.GetValueOrDefault();
            Assert.That(actual, Is.EqualTo(0));
        }

        [Test]
        public void GetValueOrDefault_ValueAvailable_ReturnsValue()
        {
            var sut = Optional.Of(5);
            var actual = sut.GetValueOrDefault();
            Assert.That(actual, Is.EqualTo(5));
        }

        [Test]
        public void GetValueOrDefaultWithParameter_ValueAvailable_ReturnsValue()
        {
            var sut = Optional.Of(5);
            var actual = sut.GetValueOrDefault(42);
            Assert.That(actual, Is.EqualTo(5));
        }

        [Test]
        public void GetValueOrDefaultWithParameter_NoValueAvailable_ReturnsInputDefaultValue()
        {
            var sut = Optional.Empty<int>();
            var actual = sut.GetValueOrDefault(42);
            Assert.That(actual, Is.EqualTo(42));
        }

        [Test]
        public void GetHashcode_NoValueAvailable_ReturnsZero()
        {
            var sut = Optional.Empty<int>();
            var actual = sut.GetHashCode();
            Assert.That(actual, Is.EqualTo(0));
        }

        [Test]
        public void GetHashcode_ValueAvailable_ReturnsHashcodeThatEqualsHashCodeOfInternValue()
        {
            var sut = Optional.Of(5);
            Assert.That(sut.GetHashCode(), Is.EqualTo(5.GetHashCode()));
        }

        [Test]
        public void Equals_ValueIsNotAvailableAndOtherParameterIsNull_ReturnsTrue()
        {
            var sut = Optional.Empty<string>();
            var actual = sut.Equals(null);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void Equals_ValueIsAvailableAndOtherOptionalObjectHasEqualValue_ReturnsTrue()
        {
            var sut = Optional.Of("Test");
            var other = Optional.Of("Test");
            var actual = sut.Equals(other);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void Equals_ValueIsAvailableAndOrdinaryObjectIsEqual_ReturnsTrue()
        {
            var sut = Optional.Of("Test");
            var other = "Test";
            var actual = sut.Equals(other);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void Equals_BothAreEmpty_ReturnsTrue()
        {
            var sut = Optional.Empty<string>();
            var other = Optional.Empty<string>();
            var actual = sut.Equals(other);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void Equals_ValueIsAvailableAndOtherIsNull_ReturnsFalse()
        {
            var sut = Optional.Of("Test");
            object other = null;
            var actual = sut.Equals(other);
            Assert.That(actual, Is.False);
        }

        [Test]
        public void Equals_ValueIsNotAvailableAndOtherIsNotNull_ReturnsFalse()
        {
            var sut = Optional.Empty<string>();
            object other = 5;
            var actual = sut.Equals(other);
            Assert.That(actual, Is.False);
        }

        [Test]
        public void Equals_BothInternalValuesAreNotEqual_ReturnsFalse()
        {
            var sut = Optional.Of("Test");
            object other = Optional.Of("Not Test");
            var actual = sut.Equals(other);
            Assert.That(actual, Is.False);
        }

        [Test]
        public void Equals_BothInternalValuesAreOfDifferentType_ReturnsFalse()
        {
            var sut = Optional.Of("Test");
            object other = Optional.Of(5);
            var actual = sut.Equals(other);
            Assert.That(actual, Is.False);
        }

        [Test]
        public void Equals_FirstHasZeroAsValueAndSecondHasNoValue_ReturnsFalse()
        {
            var first = Optional.Of(0);
            var second = Optional.Empty<int>();
            var actual = first.Equals(second);
            Assert.That(actual, Is.False);
        }

        [Test]
        public void Equals_EqualValueButDifferentStaticType_ReturnsTrue()
        {
            var first = Optional.Of(5);
            var second = Optional.Of<object>(5);
            var actual = first.Equals(second);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void Equals_BothAreEmptyAndOtherIsAssignableToSut_ReturnsTrue()
        {
            var sut = Optional.Empty<object>();
            var other = Optional.Empty<int>();
            var actual = sut.Equals(other);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void Equals_BothAreEmptyAndOtherIsNotAssignableToSut_ReturnsFalse()
        {
            var sut = Optional.Empty<int>();
            var other = Optional.Empty<object>();
            var actual = sut.Equals(other);
            Assert.That(actual, Is.False);
        }


        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        public void Tostring_HasValue_ReturnsStringFromUnderlyingObject(int value)
        {
            var sut = Optional.Of(value);
            Assert.That(sut.ToString(), Is.EqualTo(value.ToString()));
        }

        [Test]
        public void Tostring_HasNoValue_ReturnsEmptyStrinng()
        {
            var sut = Optional.Empty<int>();
            Assert.That(sut.ToString(), Is.EqualTo(string.Empty));
        }

        [Test]
        public void ImplicitOperator_ValueIsNotNull_CreatesOptionalObjectWithAssignedValue()
        {
            Optional<string> sut = "Test";
            Assert.That(sut.Value, Is.EqualTo("Test"));
        }

        [Test]
        public void ImplicitOperator_ValueNotNull_SetsSutToNull()
        {
            Optional<string> sut = null;
            Assert.That(sut, Is.Null);
        }

        [TestCase("Test")]
        [TestCase("Rest")]
        public void ExplicitOperator_AsExpected_ReturnsInnerValue(string value)
        {
            var sut = Optional<string>.Of(value);
            var actual = (string)sut;
            Assert.That(actual, Is.EqualTo(value));
        }

        [Test]
        public void ExplicitOperator_SutIsNullAndReferenceType_ReturnsDefaultValueOfInnerType()
        {
            Optional<string> sut = null;
            var actual = (string)sut;
            Assert.That(actual, Is.EqualTo(default(string)));
        }

        [Test]
        public void ExplicitOperator_SutIsNullAndValueType_ReturnsDefaultValueOfInnerType()
        {
            Optional<int> sut = null;
            var actual = (int)sut;
            Assert.That(actual, Is.EqualTo(default(int)));
        }

        [Test]
        public void Do_HasValue_ExecutesDoAction()
        {
            var mockTest = new Mock<IDoAction>();
            mockTest.Setup(x => x.Do()).Verifiable();

            Optional<int> sut = 5;
            sut.Do((x) => mockTest.Object.Do());

            mockTest.Verify();
        }

        [Test]
        public void Do_HasNoValue_DoesNotExecuteDoAction()
        {
            var mockTest = new Mock<IDoAction>();
            mockTest.Setup(x => x.Do());

            var sut = Optional.Empty<string>();
            sut.Do(_ => mockTest.Object.Do());

            mockTest.Verify(x => x.Do(), Times.Never);
        }

        [Test]
        public void DoOrElse_HasValue_ExecutesDoActionButNotElseAction()
        {
            var mockDo = new Mock<IDoAction>();
            mockDo.Setup(x => x.Do());

            var mockElse = new Mock<IElseAction>();
            mockElse.Setup(x => x.Else());

            Optional<string> sut = "Test";
            sut.DoOrElse(
                _ => mockDo.Object.Do(),
                () => mockElse.Object.Else());

            mockDo.Verify(x => x.Do());
            mockElse.Verify(x => x.Else(), Times.Never);
        }

        [Test]
        public void DoOrElse_HasNotValue_ExecutesElseActionButNotDoAction()
        {
            var mockDo = new Mock<IDoAction>();
            mockDo.Setup(x => x.Do());

            var mockElse = new Mock<IElseAction>();
            mockElse.Setup(x => x.Else());

            Optional<string> sut = Optional<string>.Empty();
            sut.DoOrElse(
                _ => mockDo.Object.Do(),
                () => mockElse.Object.Else());

            mockDo.Verify(x => x.Do(), Times.Never);
            mockElse.Verify(x => x.Else());
        }
        public interface IDoAction
        {
            void Do();
        }

        public interface IElseAction
        {
            void Else();
        }

        [Test]
        public void Select_HasNoValue_OperatesOnInnerValueAndDoesNotThrowException()
        {
            var sut = Optional<string>.Empty();
            Assert.That(() => sut.Select(x => x.Length), Throws.Nothing);
        }

        [Test]
        public void Select_HasNoVlaue_ReturnsOptionalObjectOfResultingType()
        {
            var sut = Optional<string>.Empty();
            var actual = sut.Select(x => x.Length);
            Assert.That(actual, Is.InstanceOf<Optional<int>>());
        }

        [Test]
        public void Cast_CastIsNotPossible_ThrowsInvalidCastException()
        {
            var sut = Optional.Of<object>(5);
            Assert.That(() => sut.Cast<string>(), Throws.InstanceOf<InvalidCastException>());
        }

        [Test]
        public void Cast_CastIsPossible_ReturnsNewOptionalObjectWithCastValue()
        {
            var sut = Optional.Of<object>(5);
            var actual = sut.Cast<int>();
            Assert.That(actual, Is.EqualTo(Optional.Of(5)));
            Assert.That(actual, Is.InstanceOf<Optional<int>>());
        }

        [Test]
        public void Cast_HasNoValue_ReturnsNewEmptyOptionalCast()
        {
            var sut = Optional.Empty<object>();
            var actual = sut.Cast<int>();
            Assert.That(actual, Is.InstanceOf<Optional<int>>());
        }

        [Test]
        public void GetObjectValue_HasValue_RetrunsValue()
        {
            var sut = Optional.Of("Test");
            var actual = sut.ObjectValue;
            Assert.That(actual, Is.EqualTo("Test"));
        }

        [Test]
        public void GetObjectValue_HasNoValue_RetrunsValue()
        {
            var sut = Optional.Empty<string>();
            Assert.That(() => sut.ObjectValue, Throws.InstanceOf<InvalidOperationException>());
        }

        [Test]
        public void AsOptional_ObjectIsNull_ReturnsEmptyOptional() 
        {
            string nullObj = null;
            var actual = nullObj.AsOptional();
            Assert.That(actual, Is.EqualTo(Optional.Empty<string>()));
        }

        [Test]
        public void AsOptional_ObjectIsNotNull_ReturnsEmptyOptional()
        {
            string obj = "aString";
            var actual = obj.AsOptional();
            Assert.That(actual, Is.EqualTo(Optional.Of(obj)));
        }
    }
}